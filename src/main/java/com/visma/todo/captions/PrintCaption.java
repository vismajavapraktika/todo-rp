package com.visma.todo.captions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by rokas.pranskevicius on 2016-07-29.
 */
@Service
public final class PrintCaption {
    @Autowired
    private CaptionService captionService;

    public void printCaption(String captionName){
        System.out.println(captionService.getCaption(captionName));
    }
}
