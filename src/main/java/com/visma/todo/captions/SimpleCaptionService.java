package com.visma.todo.captions;

/**
 * Created by rokas.pranskevicius on 2016-07-25.
 */

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
@Service
public class SimpleCaptionService implements CaptionService {
    final private static Map<String, String> captionMap = getDefaultCaptions();

    private static Map<String, String> getDefaultCaptions() {
        Map<String, String> captions = new HashMap<>();
        captions.put("menu.choices", "Hello, please choose:\n" +
                "1. Add a task\n" +
                "2. Print out tasks\n" +
                "3. Update/Delete a task\n" +
                "0. Quit");
        captions.put("menu.input", "Your input: ");
        captions.put("menu.invalid", "Invalid command");
        captions.put("menu.addTasks", "Please choose type of task: \n" +
                "1. Reminder\n"+
                "2. Meeting\n"+
                "3. Reserve time\n"+
                "0. Go back");
        captions.put("menu.printTasks", "Please choose what type of tasks to list: \n" +
                "1. All tasks\n"+
                "2. All tasks sorted by name\n"+
                "3. All tasks sorted by start time\n"+
                "4. Reminders\n"+
                "5. Meetings\n"+
                "6. Reserved times\n"+
                "0. Go back");
        captions.put("menu.deleteUpdate", "Would you like to update or remove an item?\n" +
                "1. Update\n" +
                "2. Remove\n" +
                "0. Go back");
        captions.put("menu.whatNext", "\n Please choose what to do next: \n 1. Remove an item \n 2. Go back \n 0. Exit program");

        captions.put("menu.reminderName", "You chose to add a Reminder.\nPlease enter name of this Reminder: ");
        captions.put("menu.reminderDate", "Please enter date of this reminder (YYYY-MM-DD): ");
        captions.put("menu.reminderTime", "Please enter time of this reminder (HH:MM): ");
        captions.put("menu.reminderNext", "\n Please choose what to do next: \n 1. Add another reminder \n 0. Go back");
        captions.put("menu.meetingName", "You chose to add a Meeting.\nPlease enter name of this Meeting: ");
        captions.put("menu.meetingDate", "Please enter date of this meeting (YYYY-MM-DD): ");
        captions.put("menu.meetingTime", "Please enter time of this meeting (HH:MM): ");
        captions.put("menu.meetingDuration", "Please enter duration of this meeting (HH:MM): ");
        captions.put("menu.meetingLocation", "Please enter location of this meeting: ");
        captions.put("menu.meetingNext", "\n Please choose what to do next: \n 1. Add another meeting \n 0. Go back");
        captions.put("menu.reservationName", "You chose to add a Reservation.\nPlease enter name of this Reservation: ");
        captions.put("menu.reservationDate", "Please enter date of this reservation (YYYY-MM-DD): ");
        captions.put("menu.reservationTime", "Please enter time of this reservation (HH:MM): ");
        captions.put("menu.reservationDuration", "Please enter duration of this reservation (HH:MM): ");
        captions.put("menu.reservationNext", "\n Please choose what to do next: \n 1. Add another reservation \n 0. Go back");

        captions.put("menu.isOverlappingMeeting", "This time is overlapping another MEETING");
        captions.put("menu.isOverlappingTask", "This time is overlapping another TASK");
        captions.put("menu.isntOverlapping", "This time is OK");

        captions.put("menu.whichToDelete", "Please choose which task to delete: ");
        captions.put("menu.whichToDeleteFromDB", "Please enter ID of a task to delete: ");
        captions.put("menu.goBack", "0. Go back");

        captions.put("menu.noSuchTask", "There is no such task \n");

        captions.put("menu.allTasks", "These are all your tasks: \n");
        captions.put("menu.allReminders", "These are all your reminders: \n");
        captions.put("menu.allMeetings", "These are all your meetings: \n");
        captions.put("menu.allReservations", "These are all your reservations: \n");

        captions.put("menu.noReminders", "There are no reminders \n");
        captions.put("menu.noMeetings", "There are no meetings \n");
        captions.put("menu.noReservations", "There are no reservations \n");

        captions.put("menu.whichToUpdateFromDB", "Please enter ID of a task to update: ");
        captions.put("menu.whatToUpdate", "Please select what to update: ");
        captions.put("menu.updateReminderReservation", " 1. Name \n 2. Start date \n 3. Duration \n 0. Go back ");
        captions.put("menu.updateMeeting", " 1. Name \n 2. Start date \n 3. Duration \n 4. Location \n 0. Go back ");
        captions.put("menu.updateName", "Please enter new name: ");
        captions.put("menu.updateStartDate", "Please enter new start date (YYYY-MM-DD): ");
        captions.put("menu.updateStartTime", "Please enter new start time (HH:MM): ");
        captions.put("menu.updateDuration", "Please enter new duration (HH:MM): ");
        captions.put("menu.updateLocation", "Please enter new location: ");

        return captions;
    }

    @Override
    public String getCaption(String key) {
        return captionMap.get(key);
    }
}