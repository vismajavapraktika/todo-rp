package com.visma.todo.data;
import org.springframework.stereotype.Service;

import java.util.Scanner;

/**
 * Created by rokas.pranskevicius on 2016-07-25.
 */

@Service
public class ConsoleInputScanner implements InputScanner {

    public String getInput() {
        final Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

}