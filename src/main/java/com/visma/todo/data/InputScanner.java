package com.visma.todo.data;

/**
 * Created by rokas.pranskevicius on 2016-07-25.
 */

public interface InputScanner {
    String getInput();
}