package com.visma.todo.jdbc;

import com.visma.todo.captions.PrintCaption;
import com.visma.todo.data.InputScanner;
import com.visma.todo.time.DateTime;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import com.visma.todo.main.CrudMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by rokas.pranskevicius on 2016-07-29.
 */
@Service
public class DBupdaterConsole {

    private  final String GET_TYPE = "SELECT type FROM alltasks where id = ?";
    private  final String GET_DURATION = "SELECT duration FROM alltasks where id = ?";
    private  final String GET_STARTDATE = "SELECT starttime FROM alltasks where id = ?";
    private  final String UPDATE_NAME = "UPDATE alltasks set name = ? WHERE id = ?";
    private  final String UPDATE_STARTDATE = "UPDATE alltasks set starttime = ? WHERE id = ?";
    private  final String UPDATE_DURATION = "UPDATE alltasks set duration = ? WHERE id = ?";
    private  final String UPDATE_ENDDATE = "UPDATE alltasks set endtime = ? WHERE id = ?";
    private  final String UPDATE_LOCATION = "UPDATE alltasks set location = ? WHERE id = ?";

    @Autowired
    private DBconnection connection;

    @Autowired
    PrintCaption captionPrinter;

    @Autowired
    private DateTime enteringDateTime;

    @Autowired
    private InputScanner inputScanner;

    @Autowired
    private CrudMenu obj;

    public void Update(int id) throws IOException, ParseException {
        id = id + 1;
        String type = getType(id);

        captionPrinter.printCaption("menu.whatToUpdate");

        switch(type){
            case "reservation":
            case "reminder":
                captionPrinter.printCaption("menu.updateReminderReservation");
                int choice = Integer.parseInt(inputScanner.getInput());
                switch (choice){
                    case 1:
                        captionPrinter.printCaption("menu.updateName");
                        String name = inputScanner.getInput();
                        updateName(id, name);
                        break;
                    case 2:
                        updateDate(id);
                        break;
                    case 3:
                        updateDuration(id);
                        break;
                    case 0:
                        obj.updateOrDelete();
                        break;
                    default:
                        System.exit(0);
                }
                break;
            case "meeting":
                captionPrinter.printCaption("menu.updateMeeting");
                choice = Integer.parseInt(inputScanner.getInput());
                switch (choice){
                    case 1:
                        captionPrinter.printCaption("menu.updateName");
                        String name = inputScanner.getInput();
                        updateName(id, name);
                        break;
                    case 2:
                        updateDate(id);
                        break;
                    case 3:
                        updateDuration(id);
                        break;
                    case 4:
                        captionPrinter.printCaption("menu.updateLocation");
                        String location = inputScanner.getInput();
                        updateLocation(id,location);
                    case 0:
                        CrudMenu obj = new CrudMenu();
                        obj.updateOrDelete();
                        break;
                    default:
                        System.exit(0);
                }
                break;
        }
    }


    public String getType(int id){
        PreparedStatement statement = null;

        try {
            Connection c = connection.connect();
            statement = c.prepareStatement(GET_TYPE);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                String type = rs.getString("type");
                return type;
            }
            rs.close();
            statement.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        System.out.println("Operation done successfully");
        return "great success";
    }

    public void updateName(int id, String name){
        PreparedStatement statement = null;
        try (Connection c = connection.connect()){
            statement = c.prepareStatement(UPDATE_NAME);
            statement.setString(1, name);
            statement.setInt(2, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    public void updateDate(int id) throws ParseException {
        captionPrinter.printCaption("menu.updateStartDate");
        String newDate = enteringDateTime.enterDate();
        captionPrinter.printCaption("menu.updateStartTime");
        String newTime = enteringDateTime.enterTime();

        String newStartTime = newDate + " " + newTime;

        PreparedStatement statement = null;
        try (Connection c = connection.connect()){
            statement = c.prepareStatement(UPDATE_STARTDATE);
            statement.setString(1, newStartTime);
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
        String duration = getDuration(id);
        updateEndTime(id, newStartTime, duration);
    }

    public void updateDuration(int id) throws ParseException {

        captionPrinter.printCaption("menu.updateDuration");
        String duration = enteringDateTime.enterTime();

        PreparedStatement statement = null;
        try (Connection c = connection.connect()) {
            statement = c.prepareStatement(UPDATE_DURATION);
            statement.setString(1, duration);
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }

        String startdate = getStartDate(id);

        // po to :
         updateEndTime(id, startdate,duration);
    }

    public void updateEndTime(int id, String newStartTime, String duration) throws ParseException {


        String newEndTime = enteringDateTime.calcDuration(newStartTime, duration);

        PreparedStatement statement = null;
        try (Connection c = connection.connect()) {
            statement = c.prepareStatement(UPDATE_ENDDATE);
            statement.setString(1, newEndTime);
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    public void updateLocation(int id, String location){
        PreparedStatement statement = null;
        try (Connection c = connection.connect()){
            statement = c.prepareStatement(UPDATE_LOCATION);
            statement.setString(1, location);
            statement.setInt(2, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    private void closeStatement(java.sql.Statement statement) {
        if(statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public String getDuration(int id){
        PreparedStatement statement = null;

        try {
            Connection c = connection.connect();
            statement = c.prepareStatement(GET_DURATION);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                String duration = rs.getString("duration");
                return duration;
            }
            rs.close();
            statement.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        System.out.println("Operation done successfully");
        return "great success";
    }

    public String getStartDate(int id){
        PreparedStatement statement = null;

        try {
            Connection c = connection.connect();
            statement = c.prepareStatement(GET_STARTDATE);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                String starttime = rs.getString("starttime");
                return starttime;
            }
            rs.close();
            statement.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        System.out.println("Operation done successfully");
        return "great success";
    }
}
