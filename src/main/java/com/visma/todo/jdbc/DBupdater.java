package com.visma.todo.jdbc;

import com.visma.todo.main.CrudMenu;
import com.visma.todo.tasks.Task;
import com.visma.todo.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * Created by rokas.pranskevicius on 2016-08-06.
 */
@Service
public class DBupdater {
    @Autowired
    private DBconnection connection;
    @Autowired
    private DateTime enteringDateTime;

    private final String UPDATE_NAME = "UPDATE alltasks set name = ? WHERE id = ?";
    private final String UPDATE_STARTDATE = "UPDATE alltasks set starttime = ? WHERE id = ?";
    private final String UPDATE_DURATION = "UPDATE alltasks set duration = ? WHERE id = ?";
    private final String UPDATE_ENDDATE = "UPDATE alltasks set endtime = ? WHERE id = ?";
    private final String UPDATE_LOCATION = "UPDATE alltasks set location = ? WHERE id = ?";

    private  final String GET_DURATION = "SELECT duration FROM alltasks where id = ?";
    private  final String GET_STARTDATE = "SELECT starttime FROM alltasks where id = ?";


    public void update(Task updatedTask, int id, String type) throws ParseException {
        switch (type) {
            case "reservation":
            case "reminder":
                if (!updatedTask.getName().isEmpty()) {
                    updateName(updatedTask, id);
                }
                if (!updatedTask.getStartTime().isEmpty()) {
                    updateDate(updatedTask, id);
                }
                if (!updatedTask.getDuration().isEmpty()) {
                    updateDuration(updatedTask, id);
                }
                break;
            case "meeting":
                if (!updatedTask.getName().isEmpty()) {
                    updateName(updatedTask, id);
                }
                if (!updatedTask.getStartTime().isEmpty()) {
                    updateDate(updatedTask, id);
                }
                if (!updatedTask.getDuration().isEmpty()) {
                    updateDuration(updatedTask, id);
                }
                if (!updatedTask.getLocation().isEmpty()){
                    updateLocation(updatedTask, id);
                }
                break;
        }
    }

    public void updateName(Task updatedTask, int id){
        PreparedStatement statement = null;
        try (Connection c = connection.connect()){
            statement = c.prepareStatement(UPDATE_NAME);
            statement.setString(1, updatedTask.getName());
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    public void updateDate(Task updatedTask, int id) throws ParseException {
        PreparedStatement statement = null;
        try (Connection c = connection.connect()){
            statement = c.prepareStatement(UPDATE_STARTDATE);
            statement.setString(1, updatedTask.getStartTime());
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }

        String duration = updatedTask.getDuration();
        updateEndTime(id, getStartDate(id), getDuration(id));
    }

    public void updateDuration(Task updatedTask, int id) throws ParseException {
        PreparedStatement statement = null;
        try (Connection c = connection.connect()){
            statement = c.prepareStatement(UPDATE_DURATION);
            statement.setString(1, updatedTask.getDuration());
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
        // po to :
        updateEndTime(id, getStartDate(id),getDuration(id));
    }



    public String getStartDate(int id){
        PreparedStatement statement = null;

        try {
            Connection c = connection.connect();
            statement = c.prepareStatement(GET_STARTDATE);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                String starttime = rs.getString("starttime");
                return starttime;
            }
            rs.close();
            statement.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        System.out.println("Operation done successfully");
        return "great success";
    }

    public String getDuration(int id){
        PreparedStatement statement = null;

        try {
            Connection c = connection.connect();
            statement = c.prepareStatement(GET_DURATION);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                String duration = rs.getString("duration");
                return duration;
            }
            rs.close();
            statement.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        System.out.println("Operation done successfully");
        return "great success";
    }

    public void updateLocation(Task updatedTask, int id){
        PreparedStatement statement = null;
        try (Connection c = connection.connect()){
            statement = c.prepareStatement(UPDATE_LOCATION);
            statement.setString(1, updatedTask.getLocation());
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    public void updateEndTime(int id, String newStartTime, String duration) throws ParseException {

        String newEndTime = enteringDateTime.calcDuration(newStartTime, duration);

        PreparedStatement statement = null;
        try (Connection c = connection.connect()) {
            statement = c.prepareStatement(UPDATE_ENDDATE);
            statement.setString(1, newEndTime);
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    private void closeStatement(java.sql.Statement statement) {
        if(statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
