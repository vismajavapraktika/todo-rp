package com.visma.todo.jdbc;

import com.visma.todo.files.Reader;
import com.visma.todo.jdbc.DBconnection;
import com.visma.todo.main.StartMenu;
import com.visma.todo.tasks.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by rokas.pranskevicius on 2016-08-02.
 */
@Service
public class DBarrayReader implements Reader {

        @Autowired
        private DBconnection connection;

        @Override
        public void readingToArray() {

            String select_all_id = "SELECT * FROM alltasks ORDER BY id ASC";
            String select_all_name = "SELECT * FROM alltasks ORDER BY name ASC";
            String select_all_time = "SELECT * FROM alltasks ORDER BY starttime ASC";

            String select_type = "SELECT * FROM alltasks WHERE type = ?";
            String select_all = "SELECT * FROM alltasks";

            PreparedStatement statement = null;
            try {
                Connection c = connection.connect();

                statement = c.prepareStatement(select_all);
                ResultSet rs = statement.executeQuery();

                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("name");
                    String starting = rs.getString("starttime");
                    String duration = rs.getString("duration");
                    String ending = rs.getString("endtime");
                    String location = rs.getString("location");
                    String type = rs.getString("type");

                    if (type.equals("meeting")) {
                        Task task = new Task(type, id, name, starting, duration, ending, location);
                        StartMenu.getTaskList().add(task);
                    }
                    else if (type.equals("reminder") || type.equals("reservation")){
                            Task task = new Task(type, id, name, starting, duration, ending);
                        StartMenu.getTaskList().add(task);
                    }
                }

                rs.close();
                statement.close();
                c.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
            }
            System.out.println("Operation done successfully");
        }
    }
