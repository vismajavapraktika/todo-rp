package com.visma.todo.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
@Service
public class DBselector {
    @Autowired
    private DBconnection connection;

    public void select(String typ, String sort) {

         String select_type = "SELECT * FROM alltasks WHERE type = ?";
         String select_all_id = "SELECT * FROM alltasks ORDER BY id ASC";
         String select_all_name = "SELECT * FROM alltasks ORDER BY name ASC";
         String select_all_time = "SELECT * FROM alltasks ORDER BY starttime ASC";

        PreparedStatement statement = null;
        try {
            Connection c = connection.connect();

            if (typ=="all" && sort=="id"){
                statement = c.prepareStatement(select_all_id);
            }
            else if (typ=="all" && sort=="name"){
                statement = c.prepareStatement(select_all_name);
            }
            else if (typ=="all" && sort== "time"){
                statement = c.prepareStatement(select_all_time);
            }
            else {

                statement = c.prepareStatement(select_type);

                statement.setString(1, typ);

            }
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String starting = rs.getString("starttime");
                String duration = rs.getString("duration");
                String ending = rs.getString("endtime");
                String location = rs.getString("location");
                String type = rs.getString("type");

                System.out.println("ID: " + id + " Name: " + name + " Starting time: " + starting + " Duration: " + duration + " Ending time: " + ending + " Location: " +
                        location + " Type: " + type);
            }
            rs.close();
            statement.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        System.out.println("Operation done successfully");
    }
}
