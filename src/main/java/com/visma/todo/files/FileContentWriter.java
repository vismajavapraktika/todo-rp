package com.visma.todo.files;

import org.springframework.stereotype.Component;

import java.io.*;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */

@Component
public class FileContentWriter {
    private PrintWriter printWriter;
    public void writeStringToFile(String task) throws IOException {
        try {
            printWriter = new PrintWriter(new FileOutputStream(new File("data.txt"), true));
            printWriter.append(task);
            printWriter.append(System.getProperty("line.separator"));
            printWriter.flush();
            printWriter.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
