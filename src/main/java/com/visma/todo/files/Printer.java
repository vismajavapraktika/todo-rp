package com.visma.todo.files;

import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Service
public interface Printer {
    void printingToFile(int whichToDelete) throws IOException;
}
