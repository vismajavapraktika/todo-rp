package com.visma.todo.files;

import com.visma.todo.tasks.Meeting;
import com.visma.todo.tasks.Reminder;
import com.visma.todo.tasks.ReserveTime;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
public interface Reader {
    void readingToArray();
}