package com.visma.todo.tasks;

import com.visma.todo.captions.PrintCaption;
import com.visma.todo.data.InputScanner;
import com.visma.todo.files.FileContentWriter;
import com.visma.todo.jdbc.DBinserter;
import com.visma.todo.main.TypeMenu;
import com.visma.todo.main.StartMenu;
import com.visma.todo.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;


/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Component
public class ReserveTimeAdder {
    @Autowired
    private FileContentWriter writingToFile;
    @Autowired
    private DBinserter insertingToDB;
    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private DateTime enteringDateTime;
    @Autowired
    private TypeMenu obj;
    @Autowired
    private PrintCaption captionPrinter;
    private static ArrayList<ReserveTime> reservesList = StartMenu.getReservesList();
    private static ArrayList<Task> taskList = StartMenu.getTaskList();

    public void addReserveTime() throws IOException, ParseException {

        captionPrinter.printCaption("menu.reservationName");
        captionPrinter.printCaption("menu.input");
        String nameOfReservation = inputScanner.getInput();

        captionPrinter.printCaption("menu.reservationDate");
        captionPrinter.printCaption("menu.input");
        String dateOfReservation = enteringDateTime.enterDate();

        captionPrinter.printCaption("menu.reservationTime");
        captionPrinter.printCaption("menu.input");
        String reservationStarts = enteringDateTime.enterTime();

        String reservationStartTime = dateOfReservation + " " + reservationStarts;

        captionPrinter.printCaption("menu.reservationDuration");
        captionPrinter.printCaption("menu.input");
        String durationOfReservation = enteringDateTime.enterTime();

        String reservationEndTime = enteringDateTime.calcDuration(reservationStartTime, durationOfReservation);

      String type = "reservation";

        ReserveTime reserve = new ReserveTime(type, nameOfReservation, reservationStartTime, durationOfReservation, reservationEndTime);

        reservesList.add(reserve);
        taskList.add(reserve);

        insertingToDB.insert(reserve);

        String wholeReservation = type + ";" + reserve.getId()+";"+nameOfReservation + ";" + reservationStartTime + ";" + durationOfReservation + ";" + reservationEndTime;

        if (nameOfReservation != null && !nameOfReservation.isEmpty()) {
            writingToFile.writeStringToFile(wholeReservation);
        }

        captionPrinter.printCaption("menu.reservationNext");
        captionPrinter.printCaption("menu.input");
        String whatNext = inputScanner.getInput();
        switch (whatNext) {
            case "1":
                addReserveTime();
                break;
            case "0":
                obj.whichType();
                break;
        }
    }
}
