package com.visma.todo.tasks;

import org.springframework.stereotype.Service;

/**
 * Created by rokas.pranskevicius on 7/19/2016.
 */
public class Meeting extends Task {

    public Meeting(String type, String name, String startTime, String duration, String endTime, String location) {
        super(type, name, startTime, duration, endTime, location);
    }
}