package com.visma.todo.tasks;

import com.visma.todo.time.DateTime;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import static com.visma.todo.time.DateTime.parseDate;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Component
public class Overlapper {
    public boolean isOverlapping(ArrayList list, String type, String startTime, String endTime) throws ParseException {
        Date startDate1 = parseDate(startTime);
        Date endDate1 = parseDate(endTime);

        switch (type) {
            case "meeting":
                ArrayList<Meeting> meetingsList = list;
                for (int i = 0; i < meetingsList.size(); i++) {
                    Date startDate2 = parseDate(meetingsList.get(i).getStartTime());
                    Date endDate2 = parseDate(meetingsList.get(i).getEndTime());
                    if ((startDate1.before(startDate2) && endDate1.after(startDate2))
                            || (startDate1.before(endDate2) && endDate1.after(endDate2))
                            || (startDate1.before(startDate2) && endDate1.after(endDate2))
                            || (startDate1.equals(startDate2) && endDate1.equals(endDate2))) {
                        return true;
                    }
                }
        }
        return false;
    }
}