package com.visma.todo.tasks;

import com.visma.todo.captions.PrintCaption;
import com.visma.todo.data.InputScanner;
import com.visma.todo.files.FileContentWriter;
import com.visma.todo.jdbc.DBinserter;
import com.visma.todo.main.TypeMenu;
import com.visma.todo.main.StartMenu;
import com.visma.todo.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Component
public class ReminderAdder {
    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private DateTime enteringDateTime;
    @Autowired
    private Overlapper overlapping;
    @Autowired
    private FileContentWriter writingToFile;
    @Autowired
    private DBinserter insertingToDB;
    @Autowired
    private PrintCaption captionPrinter;
    @Autowired
    private TypeMenu obj;

    private static ArrayList<Reminder> remindersList = StartMenu.getRemindersList();
    private static ArrayList<Task> taskList = StartMenu.getTaskList();

    public void aReminder() throws IOException, ParseException {
        captionPrinter.printCaption("menu.reminderName");
        captionPrinter.printCaption("menu.input");
        final String name = inputScanner.getInput();
        captionPrinter.printCaption("menu.reminderDate");
        captionPrinter.printCaption("menu.input");
        String dateOfReminder = enteringDateTime.enterDate();
        captionPrinter.printCaption("menu.reminderTime");
        captionPrinter.printCaption("menu.input");
        String timeOfReminder = enteringDateTime.enterTime();

        captionPrinter.printCaption("menu.meetingDuration");
        captionPrinter.printCaption("menu.input");
        String durationOfReminder = enteringDateTime.enterTime();

        String reminderStartTime = dateOfReminder + " " + timeOfReminder;

        String reminderEndTime = enteringDateTime.calcDuration(reminderStartTime, durationOfReminder);

        if (overlapping.isOverlapping(remindersList,"reminder", reminderStartTime, reminderEndTime)) {
            captionPrinter.printCaption("menu.isOverlappingMeeting");

        } else {
            captionPrinter.printCaption("menu.isntOverlapping");
        }

        String type = "reminder";

        Reminder remind = new Reminder(type, name, reminderStartTime, durationOfReminder, reminderEndTime);

        remindersList.add(remind);
        taskList.add(remind);

        insertingToDB.insert(remind);

        String wholeReminder = type +";"+ remind.getId()+";"+ name + ";" + reminderStartTime + ";" + durationOfReminder + ";" + reminderEndTime;
        System.out.println(wholeReminder);

        writingToFile.writeStringToFile(wholeReminder);

        captionPrinter.printCaption("menu.reminderNext");
        final String whatNext = inputScanner.getInput();

        switch (whatNext) {
            case "1":
                aReminder();
                break;
            case "0":
                obj.whichType();
                break;
            default:
                System.exit(0);
        }
    }
}
