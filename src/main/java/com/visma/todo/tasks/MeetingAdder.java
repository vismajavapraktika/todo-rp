package com.visma.todo.tasks;

import com.visma.todo.captions.PrintCaption;
import com.visma.todo.data.InputScanner;
import com.visma.todo.files.FileContentWriter;
import com.visma.todo.jdbc.DBinserter;
import com.visma.todo.main.TypeMenu;
import com.visma.todo.main.StartMenu;
import com.visma.todo.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by rokas.pranskevicius on 2016-07-26.
 */
@Component
public class MeetingAdder {

    @Autowired
    private InputScanner inputScanner;
    @Autowired
    @Qualifier("dateTime")
    private DateTime enteringDateTime;
    @Autowired
    private Overlapper overlapping ;
    @Autowired
    private FileContentWriter writingToFile;
    @Autowired
    private DBinserter insertingToDB;
    @Autowired
    private PrintCaption captionPrinter;
    @Autowired
    private TypeMenu obj;
    @Autowired

    private static ArrayList<Meeting> meetingsList = StartMenu.getMeetingsList();
    private static ArrayList<Task> taskList = StartMenu.getTaskList();

    public void aMeeting() throws IOException, ParseException {
        captionPrinter.printCaption("menu.meetingName");
        captionPrinter.printCaption("menu.input");
        String nameOfMeeting = inputScanner.getInput();

        captionPrinter.printCaption("menu.meetingDate");
        captionPrinter.printCaption("menu.input");
        String dateOfMeeting = enteringDateTime.enterDate();

        captionPrinter.printCaption("menu.meetingTime");
        captionPrinter.printCaption("menu.input");
        String meetingStarts = enteringDateTime.enterTime();

        String meetingStartTime = dateOfMeeting + " " + meetingStarts;

        captionPrinter.printCaption("menu.meetingDuration");
        captionPrinter.printCaption("menu.input");
        String durationOfMeeting = enteringDateTime.enterTime();

        String meetingEndTime = enteringDateTime.calcDuration(meetingStartTime, durationOfMeeting);

        if (overlapping.isOverlapping(meetingsList,"meeting", meetingStartTime, meetingEndTime)) {
            captionPrinter.printCaption("menu.isOverlappingMeeting");

        } else {
            captionPrinter.printCaption("menu.isntOverlapping");
        }

        captionPrinter.printCaption("menu.meetingLocation");
        captionPrinter.printCaption("menu.input");
        String locationOfMeeting = inputScanner.getInput();

        String type = "meeting";

        Meeting meet = new Meeting(type, nameOfMeeting, meetingStartTime, durationOfMeeting, meetingEndTime, locationOfMeeting);

        meetingsList.add(meet);

        taskList.add(meet);

        insertingToDB.insert(meet);

        String wholeMeeting = type + ";" + meet.getId()+";"+ nameOfMeeting + ";" + meetingStartTime + ";" + durationOfMeeting + ";" + meetingEndTime + ";" + locationOfMeeting;

        writingToFile.writeStringToFile(wholeMeeting);

        captionPrinter.printCaption("menu.meetingNext");
        final String whatNext = inputScanner.getInput();
        switch (whatNext) {
            case "1":
                aMeeting();
                break;
            case "0":
                obj.whichType();
                break;
            default:
                System.exit(0);
        }
    }
}
