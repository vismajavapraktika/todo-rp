package com.visma.todo.tasks;

import com.visma.todo.captions.PrintCaption;
import com.visma.todo.jdbc.DBselector;
import com.visma.todo.jdbc.DBupdaterConsole;
import com.visma.todo.main.StartMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

/**
 * Created by rokas.pranskevicius on 2016-07-29.
 */
@Service
public class Updater {
    @Autowired
    private DBupdaterConsole updating;
    @Autowired
    private DBselector selecting;
    @Autowired
    private PrintCaption captionPrinter;
    @Autowired
    private StartMenu theStart;

    private String showAll = "all";
    private String sortID = "id";
    public void updateTask() throws IOException, ParseException {
        selecting.select(showAll, sortID);
        captionPrinter.printCaption("menu.whichToUpdateFromDB");

        Scanner scanner = new Scanner(System.in);
        int whichToUpdate = Integer.parseInt(scanner.nextLine()) - 1;

        if (whichToUpdate + 1 == 0) {
            theStart.whatToDo();
        }

        if (whichToUpdate < 0) {
            captionPrinter.printCaption("menu.noSuchTask");
            updateTask();
        }
        updating.Update(whichToUpdate);
    }
}